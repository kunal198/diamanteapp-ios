//
//  ViewController.swift
//  DiamanteApp
//
//  Created by mrinal khullar on 10/13/16.
//  Copyright © 2016 mrinal khullar. All rights reserved.
//

import UIKit

 class ViewController: UIViewController , UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout
{
//OUTLETS OF SUBVIEWS
    var someProperty: AnyObject = "Some Initializer Val"
    
    @IBOutlet weak var collection_view: UICollectionView!
    @IBOutlet weak var titlelabel: UILabel!
    
    
    @IBOutlet weak var BlackBlurView: UIView!
    @IBOutlet weak var SideBarMenuView: UIView!
    @IBOutlet weak var SideBarBtn: UIButton!
    @IBOutlet weak var LyncWithHospitalBtn: UIButton!
    @IBOutlet weak var LyncWithHospitalLbl: UILabel!
    @IBOutlet weak var MyCareerGoalsBtn: UIButton!
    @IBOutlet weak var MyCareerGoalsLbl: UILabel!
    @IBOutlet weak var MyExperienceBtn: UIButton!
    @IBOutlet weak var MyExperienceLbl: UILabel!
    @IBOutlet weak var CreateProfileBtn: UIButton!
    @IBOutlet weak var CreateProfileLbl: UILabel!
    @IBOutlet weak var MyProfileLbl: UILabel!
    
    
    
    
//VARIABLES DECLARATION
    var arrayoflabel1 = [String]()
    var arrayoflabel2 = [String]()
    var arrayofimages1 = [UIImage]()
     var arrayofimages2 = [UIImage]()
    override func viewDidLoad()
    {
        super.viewDidLoad()
        

        self.automaticallyAdjustsScrollViewInsets = false
        self.navigationController?.navigationBarHidden = true
        
      //  titlelabel.font = titlelabel.font.fontWithSize(35)
        
        let flow_layout = UICollectionViewFlowLayout()
        
        flow_layout.minimumInteritemSpacing = 4
        flow_layout.minimumLineSpacing = 4
        flow_layout.sectionInset.left = 0
        flow_layout.sectionInset.right = 0
       
        collection_view.collectionViewLayout = flow_layout
        
        arrayoflabel1 = ["PROPERTY","PROGRAM","TRAVEL","VACATION","PAYMENT","WORKSHEET"]
        arrayoflabel2 = ["TOUR","BUILDER","AFFILIATES","CALCULATOR","CALCULATOR",""]
        arrayofimages1 = [UIImage(named: "homecell1")!,UIImage(named: "homecell2")!,UIImage(named: "homecell3")!,UIImage(named: "homecell4")!,UIImage(named: "homecell5")!,UIImage(named: "homecell6")!]
        arrayofimages2 = [UIImage(named: "homeblack1")!,UIImage(named: "homeblack2")!,UIImage(named: "homeblack3")!,UIImage(named: "homeblack4")!,UIImage(named: "homeblack5")!,UIImage(named: "homeblack6")!]
        
        
        var gestureRecognizer = UITapGestureRecognizer(target: self, action:Selector("swipeHandler"))
        gestureRecognizer.numberOfTapsRequired = 1
        //gestureRecognizer.direction = UISwipeGestureRecognizerDirection.Right
        self.BlackBlurView.addGestureRecognizer(gestureRecognizer)
        
        
//@selector(self.swipeHandler)
        
    }
    func swipeHandler ()
    {
        
        print("Swipe received.")
        
          self.BlackBlurView.hidden = true
        
        UIView.animateWithDuration(0.4, delay: 0.0, options: UIViewAnimationOptions.TransitionFlipFromRight, animations: { () -> Void in
            
            self.SideBarMenuView.frame.origin.x = self.view.frame.size.width
            
            }, completion: { (finished: Bool) -> Void in
                
                // you can do this in a shorter, more concise way by setting the value to its opposite, NOT value
                
                
        })

        
    }
    override func didReceiveMemoryWarning()
    {
        
        super.didReceiveMemoryWarning()
        
    }
    override func viewWillAppear(animated: Bool)
    {
        
        self.SideBarMenuView.frame.origin.x = self.view.frame.size.width
        self.BlackBlurView.hidden = true
        
    }
    
//COLLECTION VIEW DELEGATES AND DATA SOURCE
    
     func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
    
       return 6
    
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int
    {
    
        return 1
        
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
       
        
      let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell1", forIndexPath: indexPath) as! HomeCollectionViewCell
        
        cell.label1.text = arrayoflabel1[indexPath.row]
        cell.label2.text = arrayoflabel2[indexPath.row]
        cell.imageview.image = arrayofimages1[indexPath.row]
        
        cell.label1.font = cell.label1.font.fontWithSize(28)
        cell.label2.font = cell.label2.font.fontWithSize(28)
        
        return cell
        
    }

    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
    
        let screensize = self.view.frame.size
        
        let screenwidth = screensize.width
        
        let cellwiddth = screenwidth/2-2
        
        var cellheight = CGFloat()
        
        if(indexPath.row == 2 || indexPath.row == 3)
        {
            cellheight = (collection_view.frame.size.height/3) - 4
        }
        else
        {
            cellheight = (collection_view.frame.size.height/3) - 2
        }
        
        
        let rectofcell = CGSizeMake(cellwiddth, cellheight)
        
        return rectofcell
        
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
    {
        //travel
        if(indexPath.row == 0)
        {
            let obj = self.storyboard?.instantiateViewControllerWithIdentifier("imagevc") as! ImageViewController
            
            self.navigationController?.pushViewController(obj, animated: true)
        }

        else if(indexPath.row == 1)
        {
            let obj = self.storyboard?.instantiateViewControllerWithIdentifier("programbuilder") as! ProgramBuilderVC
            
            self.navigationController?.pushViewController(obj, animated: true)
        }

            else if(indexPath.row == 2)
        {
            let obj = self.storyboard?.instantiateViewControllerWithIdentifier("travel") as! TravelAffiliatesVC
            
            self.navigationController?.pushViewController(obj, animated: true)
        }
            
        else if(indexPath.row == 3)
        {
            let obj = self.storyboard?.instantiateViewControllerWithIdentifier("vacationvc") as! VacationCalculatorVC
            
            self.navigationController?.pushViewController(obj, animated: true)
        }
            
        else if(indexPath.row == 4)
        {
            let obj = self.storyboard?.instantiateViewControllerWithIdentifier("paymentvc") as! PaymentCalculatorVC
            
            self.navigationController?.pushViewController(obj, animated: true)
        }
            
        else
        {
            let obj = self.storyboard?.instantiateViewControllerWithIdentifier("worksheet") as! WorkSheetVC
            
            self.navigationController?.pushViewController(obj, animated: true)
        }
    
       
    
    }
    
    func collectionView(collectionView: UICollectionView, didHighlightItemAtIndexPath indexPath: NSIndexPath)
    {

//        var objcell = HomeCollectionViewCell()
//        objcell = collectionView.cellForItemAtIndexPath(indexPath) as! HomeCollectionViewCell
//        objcell.imageview.image = arrayofimages2[indexPath.row]
//        objcell.label1.textColor = UIColor.blackColor()
//        objcell.label2.textColor = UIColor.blackColor()
        let cell = collectionView.cellForItemAtIndexPath(indexPath) as! HomeCollectionViewCell
        cell.backgroundColor = UIColor(red: 99/255, green: 116/255, blue: 168/255, alpha: 1.0)
        cell.imageview.image = arrayofimages2[indexPath.row]
        cell.label1.textColor = UIColor.blackColor()
        cell.label2.textColor = UIColor.blackColor()
        
    }
    
    
    func collectionView(collectionView: UICollectionView, didUnhighlightItemAtIndexPath indexPath: NSIndexPath)
    {
        var objcell = HomeCollectionViewCell()
        objcell = collectionView.cellForItemAtIndexPath(indexPath) as! HomeCollectionViewCell
        objcell.imageview.image = arrayofimages1[indexPath.row]
        objcell.label1.textColor = UIColor.whiteColor()
        objcell.label2.textColor = UIColor.whiteColor()
        let cell = collectionView.cellForItemAtIndexPath(indexPath)
        cell?.backgroundColor = UIColor(red: 34/255, green: 43/255, blue: 72/255, alpha: 1.0)
    }
    
    
//BUTTON ACTIIONS
    
    
    
    @IBAction func MenuButton(sender: AnyObject)
    {
        
        self.BlackBlurView.hidden = false
        
        
        UIView.animateWithDuration(0.4, delay: 0.0, options: UIViewAnimationOptions.TransitionFlipFromRight, animations: { () -> Void in
            
            self.SideBarMenuView.frame.origin.x = self.view.frame.size.width / 2
         
            
            }, completion: { (finished: Bool) -> Void in
                
                // you can do this in a shorter, more concise way by setting the value to its opposite, NOT value
                
                
                
        })
        
        
    }
    
    
    
    
    @IBAction func userprofilebutton(sender: AnyObject)
    {
        let obj = self.storyboard?.instantiateViewControllerWithIdentifier("videocatevc") as! VideoCategoriesVC
        
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    
    
    
    @IBAction func SideBarBtnInMenu(sender: AnyObject)
    {
        print("SideBarBtnInMenu")
        
        self.BlackBlurView.hidden = true
        
        UIView.animateWithDuration(0.4, delay: 0.0, options: UIViewAnimationOptions.TransitionFlipFromRight, animations: { () -> Void in
            
            self.SideBarMenuView.frame.origin.x = self.view.frame.size.width
            
            }, completion: { (finished: Bool) -> Void in
                
                // you can do this in a shorter, more concise way by setting the value to its opposite, NOT value
                
                
        })

        
    }
    
    
    @IBAction func CreateProfileAction(sender: AnyObject)
    {
        print("CreateProfileAction")
    }
    
    
    
    @IBAction func MyExperienceAction(sender: AnyObject)
    {
        
        print("MyExperienceAction")
        
    }
    
    
    @IBAction func MyCareerGoalsAction(sender: AnyObject)
    {
        print("MyCareerGoalsAction")
    }
    
    
    @IBAction func LyncWithHospitalAction(sender: AnyObject)
    {
        print("LyncWithHospitalAction")
    }
    
    
    
    
    
    
}

