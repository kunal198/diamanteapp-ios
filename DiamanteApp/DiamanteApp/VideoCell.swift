//
//  VideoCell.swift
//  DiamanteApp
//
//  Created by mrinal khullar on 10/20/16.
//  Copyright © 2016 mrinal khullar. All rights reserved.
//

import UIKit

class VideoCell: UICollectionViewCell
{
    @IBOutlet weak var image_view: UIImageView!
    
    @IBOutlet weak var view_on_cell: UIView!
    @IBOutlet weak var label_cell: UILabel!
    @IBOutlet weak var iconimage: UIImageView!
}
