//
//  WorkSheetVC.swift
//  DiamanteApp
//
//  Created by mrinal khullar on 10/20/16.
//  Copyright © 2016 mrinal khullar. All rights reserved.
//

import UIKit

class WorkSheetVC: UIViewController
{

   
    
    @IBOutlet weak var main_view: UIView!
    
    @IBOutlet weak var myScrollView: UIScrollView!
    
    @IBOutlet weak var bottom_view: UIView!
    
    @IBOutlet weak var prime_button: UIButton!
    @IBOutlet weak var ssf_button: UIButton!
    @IBOutlet weak var annual_button: UIButton!
    @IBOutlet weak var biennial_button: UIButton!
    @IBOutlet weak var floating_buton: UIButton!
    @IBOutlet weak var fixed_button: UIButton!
    
    
    @IBOutlet weak var REPVIEW1: UIView!
    @IBOutlet weak var repview2: UIView!
    @IBOutlet weak var repview3: UIView!
    
    @IBOutlet weak var repview4: UIView!
    
    
    
    
    
    
    @IBOutlet weak var BlackBlurView: UIView!
    @IBOutlet weak var SideBarMenuView: UIView!
    @IBOutlet weak var SideBarBtn: UIButton!
    @IBOutlet weak var LyncWithHospitalBtn: UIButton!
    @IBOutlet weak var LyncWithHospitalLbl: UILabel!
    @IBOutlet weak var MyCareerGoalsBtn: UIButton!
    @IBOutlet weak var MyCareerGoalsLbl: UILabel!
    @IBOutlet weak var MyExperienceBtn: UIButton!
    @IBOutlet weak var MyExperienceLbl: UILabel!
    @IBOutlet weak var CreateProfileBtn: UIButton!
    @IBOutlet weak var CreateProfileLbl: UILabel!
    @IBOutlet weak var MyProfileLbl: UILabel!
    
    
    var buttonvar1 = 0
    var buttonvar2 = 0
    var buttonvar3 = 0
    var buttonvar4 = 0
    var buttonvar5 = 0
    var buttonvar6 = 0
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        
        self.view.userInteractionEnabled  = true
        
        main_view.userInteractionEnabled = true
        
        myScrollView.scrollEnabled = true
        
        myScrollView.userInteractionEnabled = true
        
        myScrollView.contentSize = CGSizeMake(0, self.main_view.frame.height)
        
        myScrollView.indicatorStyle = UIScrollViewIndicatorStyle.White
        
       // self.main_view.addSubview(myScrollView)
       // myScrollView.delegate = self
        //self.view.addSubview(myScrollView)
      //  main_view.addSubview(myScrollView)
       // myScrollView.addSubview(main_view)
        
        //self.main_view.addSubview(bottom_view)
        
        //self.main_view.addSubview(bottom_view)
        
        //self.myScrollView.bringSubviewToFront(bottom_view)
        
        self.prime_button.layer.borderWidth = 1
        self.ssf_button.layer.borderWidth = 1
        self.annual_button.layer.borderWidth = 1
        self.biennial_button.layer.borderWidth = 1
        self.floating_buton.layer.borderWidth = 1
        self.fixed_button.layer.borderWidth = 1
        
        
        self.REPVIEW1.layer.borderWidth = 1
        self.repview2.layer.borderWidth = 1
        self.repview3.layer.borderWidth = 1
        self.repview4.layer.borderWidth = 1
        
        var gestureRecognizer = UITapGestureRecognizer(target: self, action:Selector("swipeHandler"))
        gestureRecognizer.numberOfTapsRequired = 1
        //gestureRecognizer.direction = UISwipeGestureRecognizerDirection.Right
        self.BlackBlurView.addGestureRecognizer(gestureRecognizer)
        
        
      

        
    }
    func swipeHandler ()
    {
        
        print("Swipe received.")
        
        self.BlackBlurView.hidden = true
        
        UIView.animateWithDuration(0.4, delay: 0.0, options: UIViewAnimationOptions.TransitionFlipFromRight, animations: { () -> Void in
            
            self.SideBarMenuView.frame.origin.x = self.view.frame.size.width
            
            }, completion: { (finished: Bool) -> Void in
                
                // you can do this in a shorter, more concise way by setting the value to its opposite, NOT value
                
                
        })
        
        
    }
    override func viewWillAppear(animated: Bool)
    {
        
        self.SideBarMenuView.frame.origin.x = self.view.frame.size.width
        self.BlackBlurView.hidden = true
        
    }
    


    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
        
    }
    
    @IBAction func BackButton(sender: AnyObject)
    {
        self.navigationController?.popToRootViewControllerAnimated(true)
    }

    
    
    
    @IBAction func MenuButton(sender: AnyObject)
    {
        
       // self.createpdffromscroll(self.myScrollView, aFilename: "nameofpdf.pdf")
        
        self.BlackBlurView.hidden = false
        
        
        UIView.animateWithDuration(0.4, delay: 0.0, options: UIViewAnimationOptions.TransitionFlipFromRight, animations: { () -> Void in
            
            self.SideBarMenuView.frame.origin.x = self.view.frame.size.width / 2
            
            
            }, completion: { (finished: Bool) -> Void in
                
                // you can do this in a shorter, more concise way by setting the value to its opposite, NOT value
                
                
        })
    }
    
    
    
    @IBAction func SideBarBtnInMenu(sender: AnyObject)
    {
        print("SideBarBtnInMenu")
        
        self.BlackBlurView.hidden = true
        
        UIView.animateWithDuration(0.4, delay: 0.0, options: UIViewAnimationOptions.TransitionFlipFromRight, animations: { () -> Void in
            
            self.SideBarMenuView.frame.origin.x = self.view.frame.size.width
            
            }, completion: { (finished: Bool) -> Void in
                
                // you can do this in a shorter, more concise way by setting the value to its opposite, NOT value
                
                
        })
        
        
    }
    
    
    @IBAction func CreateProfileAction(sender: AnyObject)
    {
        print("CreateProfileAction")
    }
    
    
    
    @IBAction func MyExperienceAction(sender: AnyObject)
    {
        print("MyExperienceAction")
    }
    
    
    @IBAction func MyCareerGoalsAction(sender: AnyObject)
    {
        print("MyCareerGoalsAction")
    }
    
    
    @IBAction func LyncWithHospitalAction(sender: AnyObject)
    {
        print("LyncWithHospitalAction")
    }
    
    @IBAction func SubmitForm(sender: AnyObject)
    {
        var alert = UIAlertController(title: "THANKS", message: "Form is submitted!", preferredStyle: .Alert)
        var defaultAction = UIAlertAction(title: "OK", style: .Default, handler: {(action: UIAlertAction) -> Void in
            print("Ok Pressed")
        })
        alert.addAction(defaultAction)
        self.presentViewController(alert, animated: true, completion: { _ in })
    }
    
//    func createPDFDatafromUIView(aView: UIView) -> NSMutableData
//    {
//        // Creates a mutable data object for updating with binary data, like a byte array
//        
//        var pdfData = NSMutableData()
//        // Points the pdf converter to the mutable data object and to the UIView to be converted
//        UIGraphicsBeginPDFContextToData(pdfData, aView.bounds, nil)
//        UIGraphicsBeginPDFPage()
//        var pdfContext = UIGraphicsGetCurrentContext()
//        // draws rect to the view and thus this is captured by UIGraphicsBeginPDFContextToData
//        aView.layer.renderInContext(pdfContext!)
//        // remove PDF rendering context
//        UIGraphicsEndPDFContext()
//        return pdfData
//    }
//    
//    func createPDFfromUIView(aView: UIView, saveToDocumentsWithFileName aFilename: String) -> String
//    {
//        // Creates a mutable data object for updating with binary data, like a byte array
//        var pdfData = self.createPDFDatafromUIView(aView)
//        // Retrieves the document directories from the iOS device
//        var documentDirectories = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
//        var documentDirectory = documentDirectories[0]
//        var documentDirectoryFilename = NSURL(fileURLWithPath: documentDirectory).URLByAppendingPathComponent(aFilename).absoluteString
//        // instructs the mutable data object to write its context to a file on disk
//        pdfData.writeToFile(documentDirectoryFilename, atomically: true)
//        print("documentDirectoryFileName: \(documentDirectoryFilename)")
//        return documentDirectoryFilename
//    }
    
  //  (void)createPDFfromUIView:(UIScrollView*)aView saveToDocumentsWithFileName:(NSString*)aFilename

    func createpdffromscroll(aView:UIScrollView , aFilename: NSString)
    {
        var pdfData = NSMutableData()
        // Get Scrollview size
        var scrollSize = CGRect(x: aView.frame.origin.x, y: aView.frame.origin.y, width: aView.contentSize.width, height: aView.contentSize.height)
        // Points the pdf converter to the mutable data object and to the UIView to be converted
        UIGraphicsBeginPDFContextToData(pdfData, scrollSize, nil)
        UIGraphicsBeginPDFPage()
        var pdfContext = UIGraphicsGetCurrentContext()!
        // draws rect to the view and thus this is captured by UIGraphicsBeginPDFContextToData
        aView.layer.renderInContext(pdfContext)

        UIGraphicsEndPDFContext()
        // Retrieves the document directories from the iOS device
        var documentDirectories = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        var documentDirectory = documentDirectories[0]
        var documentDirectoryFilename = NSURL(fileURLWithPath: documentDirectory).URLByAppendingPathComponent(aFilename as String).absoluteString
        // instructs the mutable data object to write its context to a file on disk
        
        print(pdfData)
        
        pdfData.writeToFile(documentDirectoryFilename, atomically: true)
        
        print("documentDirectoryFileName: \(documentDirectoryFilename)")
        
    }

    @IBAction func primebutton(sender: AnyObject)
    {
        if(buttonvar1 == 0)
        {
            let btnImage = UIImage(named: "click_buttonimg")
            
            prime_button.setBackgroundImage(btnImage, forState: UIControlState.Normal)
            
            buttonvar1=1
        }
        else
        {
            let btnImage = UIImage(named: "")
            
            prime_button.setBackgroundImage(btnImage, forState: UIControlState.Normal)
            buttonvar1=0
        }
        
    }
    @IBAction func ssfbutton(sender: AnyObject)
    {
        if(buttonvar2 == 0)
        {
            let btnImage = UIImage(named: "click_buttonimg")
            
            ssf_button.setBackgroundImage(btnImage, forState: UIControlState.Normal)
            
            buttonvar2=1
        }
        else
        {
            let btnImage = UIImage(named: "")
            
            ssf_button.setBackgroundImage(btnImage, forState: UIControlState.Normal)
            buttonvar2=0
        }
    }
    @IBAction func annualbutton(sender: AnyObject)
    {
        if(buttonvar3 == 0)
        {
            let btnImage = UIImage(named: "click_buttonimg")
            
            annual_button.setBackgroundImage(btnImage, forState: UIControlState.Normal)
            
            buttonvar3=1
        }
        else
        {
            let btnImage = UIImage(named: "")
            
            annual_button.setBackgroundImage(btnImage, forState: UIControlState.Normal)
            buttonvar3=0
        }

    }
    @IBAction func biennialbutton(sender: AnyObject)
    {
        if(buttonvar4 == 0)
        {
            let btnImage = UIImage(named: "click_buttonimg")
            
            biennial_button.setBackgroundImage(btnImage, forState: UIControlState.Normal)
            
            buttonvar4=1
        }
        else
        {
            let btnImage = UIImage(named: "")
            
            biennial_button.setBackgroundImage(btnImage, forState: UIControlState.Normal)
            buttonvar4=0
        }

    }
    @IBAction func floatingbutton(sender: AnyObject)
    {
        if(buttonvar5 == 0)
        {
            let btnImage = UIImage(named: "click_buttonimg")
            
            floating_buton.setBackgroundImage(btnImage, forState: UIControlState.Normal)
            
            buttonvar5=1
        }
        else
        {
            let btnImage = UIImage(named: "")
            
            floating_buton.setBackgroundImage(btnImage, forState: UIControlState.Normal)
            buttonvar5=0
        }

    }
    @IBAction func fixedbutton(sender: AnyObject)
    {
        if(buttonvar6 == 0)
        {
            let btnImage = UIImage(named: "click_buttonimg")
            
            fixed_button.setBackgroundImage(btnImage, forState: UIControlState.Normal)
            
            buttonvar6=1
        }
        else
        {
            let btnImage = UIImage(named: "")
            
            fixed_button.setBackgroundImage(btnImage, forState: UIControlState.Normal)
            buttonvar6=0
        }

    }
}
