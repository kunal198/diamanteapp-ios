//
//  CategoriesViewController.swift
//  DiamanteApp
//
//  Created by mrinal khullar on 10/13/16.
//  Copyright © 2016 mrinal khullar. All rights reserved.
//

import UIKit

class CategoriesViewController: UIViewController , UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout
{
    
//OUTLETS OF SUBVIEWS
    
    @IBOutlet weak var collection_view: UICollectionView!
    
    
    
    
    @IBOutlet weak var BlackBlurView: UIView!
    @IBOutlet weak var SideBarMenuView: UIView!
    @IBOutlet weak var SideBarBtn: UIButton!
    @IBOutlet weak var LyncWithHospitalBtn: UIButton!
    @IBOutlet weak var LyncWithHospitalLbl: UILabel!
    @IBOutlet weak var MyCareerGoalsBtn: UIButton!
    @IBOutlet weak var MyCareerGoalsLbl: UILabel!
    @IBOutlet weak var MyExperienceBtn: UIButton!
    @IBOutlet weak var MyExperienceLbl: UILabel!
    @IBOutlet weak var CreateProfileBtn: UIButton!
    @IBOutlet weak var CreateProfileLbl: UILabel!
    @IBOutlet weak var MyProfileLbl: UILabel!

//VARIABLES DECLARATION
    var arrayoflabel1 = [String]()
    var arrayoflabel2 = [String]()
    var arrayofimages = [UIImage]()
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.automaticallyAdjustsScrollViewInsets = false
        let flow_layout = UICollectionViewFlowLayout()
        
        flow_layout.minimumInteritemSpacing = 4
        flow_layout.minimumLineSpacing = 4
        flow_layout.sectionInset.left = 0
        flow_layout.sectionInset.right = 0
        
        collection_view.collectionViewLayout = flow_layout
        
        arrayoflabel1 = ["THE RESORT","DUNES CLUB","OCEAN CLUB","GOLF VILLA RESIDENCE","DUNES","EL"]
        arrayoflabel2 = ["CLUB","RESIDENCE","RESIDENCE","CLUB","COARSE","CARDONAL"]
        arrayofimages = [UIImage(named: "homecell1")!,UIImage(named: "homecell2")!,UIImage(named: "homecell3")!,UIImage(named: "homecell4")!,UIImage(named: "homecell5")!,UIImage(named: "homecell6")!]
        
        var gestureRecognizer = UITapGestureRecognizer(target: self, action:Selector("swipeHandler"))
        gestureRecognizer.numberOfTapsRequired = 1
        //gestureRecognizer.direction = UISwipeGestureRecognizerDirection.Right
        self.BlackBlurView.addGestureRecognizer(gestureRecognizer)
        
    }
    func swipeHandler ()
    {
        
        print("Swipe received.")
        
        self.BlackBlurView.hidden = true
        
        UIView.animateWithDuration(0.4, delay: 0.0, options: UIViewAnimationOptions.TransitionFlipFromRight, animations: { () -> Void in
            
            self.SideBarMenuView.frame.origin.x = self.view.frame.size.width
            
            }, completion: { (finished: Bool) -> Void in
                
                // you can do this in a shorter, more concise way by setting the value to its opposite, NOT value
                
                
        })
        
        
    }
    override func didReceiveMemoryWarning()
    {
        
        super.didReceiveMemoryWarning()
        
    }
    override func viewWillAppear(animated: Bool)
    {
        
        self.SideBarMenuView.frame.origin.x = self.view.frame.size.width
        self.BlackBlurView.hidden = true
        
    }
    
//COLLECTION VIEW DELEGATE AND DATA SOURCE

    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        
        return 6
        
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int
    {
        
        return 1
        
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell2", forIndexPath: indexPath) as! CategoriesCollectionViewCell
        
        cell.label1.text = arrayoflabel1[indexPath.row]
        cell.label2.text = arrayoflabel2[indexPath.row]
        cell.image_view.image = arrayofimages[indexPath.row]
        
        return cell
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        
        let screensize = self.view.frame.size
        
        let screenwidth = screensize.width
        
        let cellwiddth = screenwidth/2-2
        
        var cellheight = CGFloat()
        
        if(indexPath.row == 2 || indexPath.row == 3)
        {
            cellheight = (collection_view.frame.size.height/3) - 4
        }
        else
        {
            cellheight = (collection_view.frame.size.height/3) - 2
        }
        
        
        let rectofcell = CGSizeMake(cellwiddth, cellheight)
        
        return rectofcell
        
    }
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
    {
        
        
        
        let obj = self.storyboard?.instantiateViewControllerWithIdentifier("rootvc") as! RootViewController
        obj.indexofvc = indexPath.row
        if(indexPath.row == 0)
        {
            num0 = 1
        }
            
        else if(indexPath.row == 1)
        {
            num1 = 1
        }
            
        else if(indexPath.row == 2)
        {
            num2 = 1
        }
            
        else if(indexPath.row == 3)
        {
            num3 = 1
        }
            
        else if(indexPath.row == 4)
        {
            num4 = 1
        }
            
        else
        {
            num5 = 1
        }

        self.navigationController?.pushViewController(obj, animated: true)

//        if(indexPath.row == 0)
//        {
//            
//            let obj = self.storyboard?.instantiateViewControllerWithIdentifier("trclubvc") as! TheResortClubVC
//            
//            self.navigationController?.pushViewController(obj, animated: true)
//            
//        }
//        else if(indexPath.row == 1)
//        {
//            
//            let obj = self.storyboard?.instantiateViewControllerWithIdentifier("dunesvc") as! DunesResidenceVC
//            
//            self.navigationController?.pushViewController(obj, animated: true)
//            
//        }
//        else if(indexPath.row == 2)
//        {
//            
//            let obj = self.storyboard?.instantiateViewControllerWithIdentifier("oceanclubvc") as! OceanClubVC
//            
//            self.navigationController?.pushViewController(obj, animated: true)
//            
//        }
//        else if(indexPath.row == 3)
//        {
//            
//            let obj = self.storyboard?.instantiateViewControllerWithIdentifier("golfvillavc") as! GolfVillaVC
//            
//            self.navigationController?.pushViewController(obj, animated: true)
//            
//        }
//        else if(indexPath.row == 4)
//        {
//            
//            let obj = self.storyboard?.instantiateViewControllerWithIdentifier("dunescoarsevc") as! DunesCoarseVC
//            
//            self.navigationController?.pushViewController(obj, animated: true)
//            
//        }
//        else if(indexPath.row == 5)
//        {
//            
//            let obj = self.storyboard?.instantiateViewControllerWithIdentifier("elcardonalvc") as! ELCardonalVC
//            
//            self.navigationController?.pushViewController(obj, animated: true)
//            
//        }
//        
        
    }
    
    
    @IBAction func BackButton(sender: AnyObject)
    {
        
        self.navigationController?.popViewControllerAnimated(true)
        
    }
    
    @IBAction func MenuButton(sender: AnyObject)
    {
        
        self.BlackBlurView.hidden = false
        
        
        UIView.animateWithDuration(0.4, delay: 0.0, options: UIViewAnimationOptions.TransitionFlipFromRight, animations: { () -> Void in
            
            self.SideBarMenuView.frame.origin.x = self.view.frame.size.width / 2
            
            
            }, completion: { (finished: Bool) -> Void in
                
                // you can do this in a shorter, more concise way by setting the value to its opposite, NOT value
                
                
                
        })
        
    }
    
    @IBAction func SideBarBtnInMenu(sender: AnyObject)
    {
        print("SideBarBtnInMenu")
        
        self.BlackBlurView.hidden = true
        
        UIView.animateWithDuration(0.4, delay: 0.0, options: UIViewAnimationOptions.TransitionFlipFromRight, animations: { () -> Void in
            
            self.SideBarMenuView.frame.origin.x = self.view.frame.size.width
            
            }, completion: { (finished: Bool) -> Void in
                
                // you can do this in a shorter, more concise way by setting the value to its opposite, NOT value
                
                
        })
        
        
    }
    
    
    @IBAction func CreateProfileAction(sender: AnyObject)
    {
        
        print("CreateProfileAction")
        
    }
    
    
    
    @IBAction func MyExperienceAction(sender: AnyObject)
    {
        
        print("MyExperienceAction")
        
    }
    
    
    @IBAction func MyCareerGoalsAction(sender: AnyObject)
    {
        
        print("MyCareerGoalsAction")
        
    }
    
    
    @IBAction func LyncWithHospitalAction(sender: AnyObject)
    {
        
        print("LyncWithHospitalAction")
        
    }
    
    

}
