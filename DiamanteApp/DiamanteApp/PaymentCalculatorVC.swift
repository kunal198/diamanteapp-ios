//
//  PaymentCalculatorVC.swift
//  DiamanteApp
//
//  Created by mrinal khullar on 10/18/16.
//  Copyright © 2016 mrinal khullar. All rights reserved.
//

import UIKit

class PaymentCalculatorVC: UIViewController , UITextFieldDelegate
{

    @IBOutlet weak var textfield1: UITextField!
    @IBOutlet weak var textfield2: UITextField!
    @IBOutlet weak var textfield3: UITextField!
    @IBOutlet weak var textfield4: UITextField!
    @IBOutlet weak var textfield5: UITextField!
    @IBOutlet weak var todayratebutton: UIButton!
    
    @IBOutlet weak var BlackBlurView: UIView!
    @IBOutlet weak var SideBarMenuView: UIView!
    @IBOutlet weak var SideBarBtn: UIButton!
    @IBOutlet weak var LyncWithHospitalBtn: UIButton!
    @IBOutlet weak var LyncWithHospitalLbl: UILabel!
    @IBOutlet weak var MyCareerGoalsBtn: UIButton!
    @IBOutlet weak var MyCareerGoalsLbl: UILabel!
    @IBOutlet weak var MyExperienceBtn: UIButton!
    @IBOutlet weak var MyExperienceLbl: UILabel!
    @IBOutlet weak var CreateProfileBtn: UIButton!
    @IBOutlet weak var CreateProfileLbl: UILabel!
    @IBOutlet weak var MyProfileLbl: UILabel!
    
    @IBOutlet weak var white_view: UIView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.textfield1.borderStyle = UITextBorderStyle.RoundedRect
        self.textfield2.borderStyle = UITextBorderStyle.RoundedRect
        self.textfield3.borderStyle = UITextBorderStyle.RoundedRect
        self.textfield4.borderStyle = UITextBorderStyle.RoundedRect
        self.textfield5.borderStyle = UITextBorderStyle.RoundedRect
        
        
        self.todayratebutton.layer.cornerRadius = 5
        
        
        var gestureRecognizer = UITapGestureRecognizer(target: self, action:Selector("swipeHandler"))
        gestureRecognizer.numberOfTapsRequired = 1
        //gestureRecognizer.direction = UISwipeGestureRecognizerDirection.Right
        self.BlackBlurView.addGestureRecognizer(gestureRecognizer)
        
    }
    func swipeHandler ()
    {
        
        print("Swipe received.")
        
        self.BlackBlurView.hidden = true
        
        UIView.animateWithDuration(0.4, delay: 0.0, options: UIViewAnimationOptions.TransitionFlipFromRight, animations: { () -> Void in
            
            self.SideBarMenuView.frame.origin.x = self.view.frame.size.width
            
            }, completion: { (finished: Bool) -> Void in
                
                // you can do this in a shorter, more concise way by setting the value to its opposite, NOT value
                
                
        })
        
        
    }
    override func viewWillAppear(animated: Bool)
    {
        
        self.SideBarMenuView.frame.origin.x = self.view.frame.size.width
        self.BlackBlurView.hidden = true
        
    }
    
    
    


    override func didReceiveMemoryWarning()
    {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    

    @IBAction func BackButton(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func MenuButton(sender: AnyObject)
    {
        
      //  self.createPDFfromUIView(self.white_view, saveToDocumentsWithFileName: "abcd.pdf")
        
        self.BlackBlurView.hidden = false
        
        
        UIView.animateWithDuration(0.4, delay: 0.0, options: UIViewAnimationOptions.TransitionFlipFromRight, animations: { () -> Void in
            
            self.SideBarMenuView.frame.origin.x = self.view.frame.size.width / 2
            
            
            }, completion: { (finished: Bool) -> Void in
                
                // you can do this in a shorter, more concise way by setting the value to its opposite, NOT value
                
                
                
        })
    }
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
        
    }

    
    
    @IBAction func SideBarBtnInMenu(sender: AnyObject)
    {
        print("SideBarBtnInMenu")
        
        self.BlackBlurView.hidden = true
        
        UIView.animateWithDuration(0.4, delay: 0.0, options: UIViewAnimationOptions.TransitionFlipFromRight, animations: { () -> Void in
            
            self.SideBarMenuView.frame.origin.x = self.view.frame.size.width
            
            }, completion: { (finished: Bool) -> Void in
                
                // you can do this in a shorter, more concise way by setting the value to its opposite, NOT value
                
                
        })
        
        
    }
    
    
    @IBAction func CreateProfileAction(sender: AnyObject)
    {
        print("CreateProfileAction")
    }
    
    
    
    @IBAction func MyExperienceAction(sender: AnyObject)
    {
        print("MyExperienceAction")
    }
    
    
    @IBAction func MyCareerGoalsAction(sender: AnyObject)
    {
        print("MyCareerGoalsAction")
    }
    
    
    @IBAction func LyncWithHospitalAction(sender: AnyObject)
    {
        print("LyncWithHospitalAction")
    }
    
    
//    func createPDFfromUIView(aView: UIView, aFilename: NSString)
//    {
//        var pdfData = NSMutableData()
//        
//        UIGraphicsBeginPDFContextToData(pdfData, aView.bounds, nil)
//        
//        UIGraphicsBeginPDFPage()
//        
//        var pdfContext = UIGraphicsGetCurrentContext()!
//        
//        aView.layer.renderInContext(pdfContext)
//        
//        UIGraphicsEndPDFContext()
//        
//        var documentDirectories = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
//        var documentDirectory = documentDirectories[0]
//        var documentDirectoryFilename = NSURL(fileURLWithPath: documentDirectory).URLByAppendingPathComponent(aFilename as String).absoluteString
//        
//        pdfData.writeToFile(documentDirectoryFilename, atomically: true)
//        print("documentDirectoryFileName: \(documentDirectoryFilename)")
//        
//    }
//    func createPdfFromView(aView: UIView, saveToDocumentsWithFileName fileName: String)
//    {
//        let pdfData = NSMutableData()
//        UIGraphicsBeginPDFContextToData(pdfData, aView.bounds, nil)
//        UIGraphicsBeginPDFPage()
//        
//        guard let pdfContext = UIGraphicsGetCurrentContext() else { return }
//        
//        aView.layer.renderInContext(pdfContext)
//        UIGraphicsEndPDFContext()
//        
//        if let documentDirectories = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true).first {
//            let documentsFileName = documentDirectories + "/" + fileName
//            debugPrint(documentsFileName)
//            pdfData.writeToFile(documentsFileName, atomically: true)
//        }
//    }
    
    func createPDFDatafromUIView(aView: UIView) -> NSMutableData
    {
        // Creates a mutable data object for updating with binary data, like a byte array
        
        var pdfData = NSMutableData()
        // Points the pdf converter to the mutable data object and to the UIView to be converted
        UIGraphicsBeginPDFContextToData(pdfData, aView.bounds, nil)
        UIGraphicsBeginPDFPage()
        var pdfContext = UIGraphicsGetCurrentContext()
        // draws rect to the view and thus this is captured by UIGraphicsBeginPDFContextToData
        aView.layer.renderInContext(pdfContext!)
        // remove PDF rendering context
        UIGraphicsEndPDFContext()
        return pdfData
    }
    
    func createPDFfromUIView(aView: UIView, saveToDocumentsWithFileName aFilename: String) -> String
    {
        // Creates a mutable data object for updating with binary data, like a byte array
        var pdfData = self.createPDFDatafromUIView(aView)
        // Retrieves the document directories from the iOS device
        var documentDirectories = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        var documentDirectory = documentDirectories[0]
        var documentDirectoryFilename = NSURL(fileURLWithPath: documentDirectory).URLByAppendingPathComponent(aFilename).absoluteString
        // instructs the mutable data object to write its context to a file on disk
        pdfData.writeToFile(documentDirectoryFilename, atomically: true)
        print("documentDirectoryFileName: \(documentDirectoryFilename)")
        return documentDirectoryFilename
    }
    
}
