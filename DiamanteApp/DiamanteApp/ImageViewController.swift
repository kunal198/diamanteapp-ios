//
//  ImageViewController.swift
//  DiamanteApp
//
//  Created by mrinal khullar on 10/13/16.
//  Copyright © 2016 mrinal khullar. All rights reserved.
//

import UIKit

class ImageViewController: UIViewController,UIScrollViewDelegate
{
// OUTLETS OF SUBVIEWS
    
    @IBOutlet weak var SCROLL_VIEW: UIScrollView!
    @IBOutlet weak var titlelabel: UILabel!
    @IBOutlet weak var image_view: UIImageView!
    override func viewDidLoad()
    {
        
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.navigationController?.navigationBarHidden = true
        
        self.SCROLL_VIEW.minimumZoomScale = 1
        self.SCROLL_VIEW.maximumZoomScale = 2.3
        self.SCROLL_VIEW.contentSize = self.image_view!.frame.size
        self.SCROLL_VIEW.delegate! = self
        
    }

    override func didReceiveMemoryWarning()
    {
        
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func BackButton(sender: AnyObject)
    {
        
        self.navigationController?.popViewControllerAnimated(true)
        
    }

    @IBAction func MenuButton(sender: AnyObject)
    {
        
        let obj = self.storyboard?.instantiateViewControllerWithIdentifier("categoriesvc") as! CategoriesViewController
        self.navigationController?.pushViewController(obj, animated: true)
        
    }

    
// SCROLL VIEW DELEGATES
    
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView?
    {
        
        return self.image_view!
        
    }
    
    func scrollViewDidEndZooming(scrollView: UIScrollView, withView view: UIView?, atScale scale: CGFloat)
    {
        
    }
}
