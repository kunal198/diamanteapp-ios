//
//  TravelAffiliatesVC.swift
//  DiamanteApp
//
//  Created by mrinal khullar on 10/20/16.
//  Copyright © 2016 mrinal khullar. All rights reserved.
//

import UIKit

class TravelAffiliatesVC: UIViewController , SKPSMTPMessageDelegate
{

   
   var emailMessage = SKPSMTPMessage()
    
    
    @IBOutlet weak var textview1: UITextView!
    @IBOutlet weak var textview2: UITextView!
    
    
    
    @IBOutlet weak var BlackBlurView: UIView!
    @IBOutlet weak var SideBarMenuView: UIView!
    @IBOutlet weak var SideBarBtn: UIButton!
    @IBOutlet weak var LyncWithHospitalBtn: UIButton!
    @IBOutlet weak var LyncWithHospitalLbl: UILabel!
    @IBOutlet weak var MyCareerGoalsBtn: UIButton!
    @IBOutlet weak var MyCareerGoalsLbl: UILabel!
    @IBOutlet weak var MyExperienceBtn: UIButton!
    @IBOutlet weak var MyExperienceLbl: UILabel!
    @IBOutlet weak var CreateProfileBtn: UIButton!
    @IBOutlet weak var CreateProfileLbl: UILabel!
    @IBOutlet weak var MyProfileLbl: UILabel!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.textview1.textContainer.maximumNumberOfLines = 5
        self.textview2.textContainer.maximumNumberOfLines = 5
        
        var paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.headIndent = 0
        // <--- indention if you need it
        paragraphStyle.firstLineHeadIndent = 0
        paragraphStyle.lineSpacing = 12
     //   paragraphStyle.alignment = NSTextAlignment.Right
                // <--- magic line spacing here!
        var attrsDictionary = [NSParagraphStyleAttributeName: paragraphStyle]
        // <-- there are many more attrs, e.g NSFontAttributeName
   
  //TEXT FIELD 1 WORK
        
        let title1 = NSMutableAttributedString(string: self.textview1.text, attributes: attrsDictionary)
        var font1 = UIFont(name: "Cochin", size: 17)
        
        title1.addAttribute(NSFontAttributeName, value: font1!, range: NSRange(location: 0, length: title1.string.characters.count))
        self.textview1.attributedText = title1
        self.textview1.textColor = UIColor(red: 99/255, green: 116/255, blue: 168/255, alpha: 1.0)
        
        
  //TEXT FIELD 2 WORK
        
        
        let title2 = NSMutableAttributedString(string: self.textview2.text, attributes: attrsDictionary)
        var font2 = UIFont(name: "Cochin", size: 17)
        
        title2.addAttribute(NSFontAttributeName, value: font2!, range: NSRange(location: 0, length: title2.string.characters.count))
        self.textview2.attributedText = title2
        self.textview2.textColor = UIColor(red: 99/255, green: 116/255, blue: 168/255, alpha: 1.0)
        
        self.textview2.textAlignment = NSTextAlignment.Right
        
        
        
        
        var gestureRecognizer = UITapGestureRecognizer(target: self, action:Selector("swipeHandler"))
        gestureRecognizer.numberOfTapsRequired = 1
        //gestureRecognizer.direction = UISwipeGestureRecognizerDirection.Right
        self.BlackBlurView.addGestureRecognizer(gestureRecognizer)
        
    }
    func swipeHandler ()
    {
        
        print("Swipe received.")
        
        self.BlackBlurView.hidden = true
        
        UIView.animateWithDuration(0.4, delay: 0.0, options: UIViewAnimationOptions.TransitionFlipFromRight, animations: { () -> Void in
            
            self.SideBarMenuView.frame.origin.x = self.view.frame.size.width
            
            }, completion: { (finished: Bool) -> Void in
                
                // you can do this in a shorter, more concise way by setting the value to its opposite, NOT value
                
                
        })
        
        
    }
    override func viewWillAppear(animated: Bool)
    {
        
        self.SideBarMenuView.frame.origin.x = self.view.frame.size.width
        self.BlackBlurView.hidden = true
        
    }
    


    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func BackButton(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func MenuButton(sender: AnyObject)
    {
        
        sendEmailInBackground()
        
//        self.BlackBlurView.hidden = false
//        
//        
//        UIView.animateWithDuration(0.4, delay: 0.0, options: UIViewAnimationOptions.TransitionFlipFromRight, animations: { () -> Void in
//            
//            self.SideBarMenuView.frame.origin.x = self.view.frame.size.width / 2
//            
//            
//            }, completion: { (finished: Bool) -> Void in
//                
//                // you can do this in a shorter, more concise way by setting the value to its opposite, NOT value
//                
//                
//                
//        })

    }
    
    @IBAction func SideBarBtnInMenu(sender: AnyObject)
    {
        print("SideBarBtnInMenu")
        
        self.BlackBlurView.hidden = true
        
        UIView.animateWithDuration(0.4, delay: 0.0, options: UIViewAnimationOptions.TransitionFlipFromRight, animations: { () -> Void in
            
            self.SideBarMenuView.frame.origin.x = self.view.frame.size.width
            
            }, completion: { (finished: Bool) -> Void in
                
                // you can do this in a shorter, more concise way by setting the value to its opposite, NOT value
                
                
        })
        
        
    }
    
    
    @IBAction func CreateProfileAction(sender: AnyObject)
    {
        print("CreateProfileAction")
    }
    
    
    
    @IBAction func MyExperienceAction(sender: AnyObject)
    {
        print("MyExperienceAction")
    }
    
    
    @IBAction func MyCareerGoalsAction(sender: AnyObject)
    {
        print("MyCareerGoalsAction")
    }
    
    
    @IBAction func LyncWithHospitalAction(sender: AnyObject)
    {
        print("LyncWithHospitalAction")
    }
    
    func sendEmailInBackground()
    {
    print("Start Sending")
    
        
        emailMessage.fromEmail = "brstdev3@gmail.com"
        
        emailMessage.toEmail = "brstdev3@gmail.com"
        
        emailMessage.relayHost = "smtp.gmail.com"
        
        emailMessage.requiresAuth = true
        
        emailMessage.login = "brstdev3@gmail.com"
        
        emailMessage.pass = "brstdeveloper131"
        
    emailMessage.subject = "subject"
    var message = "email subject header message";
    emailMessage.wantsSecure = true
        
    emailMessage.delegate = self
    var messageBody = "your email body"
        var vcfPath = NSBundle.mainBundle().pathForResource("20141110103313108", ofType: "pdf")
        var vcfData = NSData(contentsOfFile: vcfPath!)
        var vcfPart = [
            kSKPSMTPPartContentTypeKey : "pdf/directory;\r\n\tx-unix-mode=0644;\r\n\tname=\"20141110103313108.pdf\"",
            kSKPSMTPPartContentDispositionKey : "attachment;\r\n\tfilename=\"20141110103313108.pdf\"",
            kSKPSMTPPartMessageKey : (vcfData?.encodeBase64ForData())!,
            kSKPSMTPPartContentTransferEncodingKey : "base64"
        ] as NSMutableDictionary
        
       
        
        var plainMsg = [
            kSKPSMTPPartContentTypeKey : "text/plain",
            kSKPSMTPPartMessageKey : messageBody,
            kSKPSMTPPartContentTransferEncodingKey : "8bit"
        ]
        emailMessage.parts = [vcfPart,plainMsg]
     //   emailMessage.parts = [plainMsg]
        emailMessage.send()
    }
    
    func messageSent(_ message: SKPSMTPMessage)
    {
        
        print("delegate - message sent")
        var alert = UIAlertView(title: "Message sent.", message: "", delegate: nil, cancelButtonTitle: "Ok", otherButtonTitles: "")
        alert.show()
        
    }
    func messageFailed(_ message: SKPSMTPMessage, error: NSError?)
    {
        // open an alert with just an OK button
        var alert = UIAlertView(title: "Error!", message: error!.localizedDescription, delegate: nil, cancelButtonTitle: "Ok", otherButtonTitles: "")
        alert.show()
        print("delegate - error(\(error!.code)): \(error!.localizedDescription)")
    }
}
