//
//  VideoDescriptionVC.swift
//  DiamanteApp
//
//  Created by mrinal khullar on 10/20/16.
//  Copyright © 2016 mrinal khullar. All rights reserved.
//

import UIKit
import MediaPlayer
import MobileCoreServices
import AVKit

class VideoDescriptionVC: UIViewController
{
    
    var image_var = UIImage()
    
    var player = AVPlayer()
    
    var controller = AVPlayerViewController()
    
   // var playercontroller = AVPlayerViewController()

    @IBOutlet weak var white_view: UIView!
    
    @IBOutlet weak var image_view: UIImageView!
    
    @IBOutlet weak var BlackBlurView: UIView!
    @IBOutlet weak var SideBarMenuView: UIView!
    @IBOutlet weak var SideBarBtn: UIButton!
    @IBOutlet weak var LyncWithHospitalBtn: UIButton!
    @IBOutlet weak var LyncWithHospitalLbl: UILabel!
    @IBOutlet weak var MyCareerGoalsBtn: UIButton!
    @IBOutlet weak var MyCareerGoalsLbl: UILabel!
    @IBOutlet weak var MyExperienceBtn: UIButton!
    @IBOutlet weak var MyExperienceLbl: UILabel!
    @IBOutlet weak var CreateProfileBtn: UIButton!
    @IBOutlet weak var CreateProfileLbl: UILabel!
    @IBOutlet weak var MyProfileLbl: UILabel!
    
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
      //  UIApplication.sharedApplication().keyWindow?.bringSubviewToFront(SideBarMenuView)
        
        image_view.image = image_var
        
        self.white_view.layer.cornerRadius = 4
        
        
        var gestureRecognizer = UITapGestureRecognizer(target: self, action:Selector("swipeHandler"))
        gestureRecognizer.numberOfTapsRequired = 1
        //gestureRecognizer.direction = UISwipeGestureRecognizerDirection.Right
        self.BlackBlurView.addGestureRecognizer(gestureRecognizer)
        
    }
    func swipeHandler ()
    {
        
        print("Swipe received.")
        
        self.BlackBlurView.hidden = true
        
        UIView.animateWithDuration(0.4, delay: 0.0, options: UIViewAnimationOptions.TransitionFlipFromRight, animations: { () -> Void in
            
            self.SideBarMenuView.frame.origin.x = self.view.frame.size.width
            
            }, completion: { (finished: Bool) -> Void in
                
                // you can do this in a shorter, more concise way by setting the value to its opposite, NOT value
                
                
        })
        
        
    }
    override func viewWillAppear(animated: Bool)
    {
        
        self.SideBarMenuView.frame.origin.x = self.view.frame.size.width
        self.BlackBlurView.hidden = true
        
    }


    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func BackButton(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    @IBAction func MenuButton(sender: AnyObject)
    {
        
//        let path = NSBundle.mainBundle().pathForResource("Diamante Family", ofType:"mp4")
//        
//        let videoURL = NSURL(fileURLWithPath: path!)
//        let player1 = AVPlayer(URL: videoURL)
//        let playerViewController = AVPlayerViewController()
//        playerViewController.player = player1
//        //playerViewController.view.frame = CGRectMake(50, 100, 150, 150)
//        self.presentViewController(playerViewController, animated: true)
//            {
//            playerViewController.player!.play()
//        }
        
        
        
        
        self.BlackBlurView.hidden = false
        
        
        UIView.animateWithDuration(0.4, delay: 0.0, options: UIViewAnimationOptions.TransitionFlipFromRight, animations: { () -> Void in
            
            self.SideBarMenuView.frame.origin.x = self.view.frame.size.width / 2
            
            
            }, completion: { (finished: Bool) -> Void in
                
                // you can do this in a shorter, more concise way by setting the value to its opposite, NOT value
                
                
                
        })
        
       
        
//        let path = NSBundle.mainBundle().pathForResource("Diamante Family", ofType:"mp4")
//        
//        player = AVPlayer(URL: NSURL(fileURLWithPath: path!))
//        let playerLayer = AVPlayerLayer(player: player)
//        playerLayer.frame = self.image_view.frame
//        playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
//        self.view.layer.addSublayer(playerLayer)
//        player.seekToTime(kCMTimeZero)
//        player.play()
//        player.actionAtItemEnd = AVPlayerActionAtItemEnd.None
        

        
        
        
    }
    
    func playerItemDidReachEnd(notification: NSNotification)
    {
        
        print("end video")
        
       //self.controller.removeFromParentViewController()
        
        print(self.childViewControllers.count)
//        self.childViewControllers[0].willMoveToParentViewController(nil)
//        self.childViewControllers[0].view.removeFromSuperview()
//        self.childViewControllers[0].removeFromParentViewController()
        
    }
    
    @IBAction func SideBarBtnInMenu(sender: AnyObject)
    {
        
        
        print("SideBarBtnInMenu")
        
        self.BlackBlurView.hidden = true
        
        UIView.animateWithDuration(0.4, delay: 0.0, options: UIViewAnimationOptions.TransitionFlipFromRight, animations: { () -> Void in
            
            self.SideBarMenuView.frame.origin.x = self.view.frame.size.width
            
            }, completion: { (finished: Bool) -> Void in
                
                // you can do this in a shorter, more concise way by setting the value to its opposite, NOT value
                
                
        })
        
        
    }
    
    
    @IBAction func CreateProfileAction(sender: AnyObject)
    {
        print("CreateProfileAction")
    }
    
    
    
    @IBAction func MyExperienceAction(sender: AnyObject)
    {
        print("MyExperienceAction")
    }
    
    
    @IBAction func MyCareerGoalsAction(sender: AnyObject)
    {
        print("MyCareerGoalsAction")
    }
    
    
    @IBAction func LyncWithHospitalAction(sender: AnyObject)
    {
        print("LyncWithHospitalAction")
    }
    
    
    @IBAction func PlayButton(sender: AnyObject)
    {
        
        let path = NSBundle.mainBundle().pathForResource("El Cardonal Video", ofType:"mp4")
        
        let videoURL = NSURL(fileURLWithPath: path!)
        
        self.controller.view.superview?.bringSubviewToFront(SideBarMenuView)
        
        var video = AVPlayer(URL: videoURL)
        
        controller.player = video
        self.view!.addSubview(controller.view)
        controller.view.frame = self.image_view.frame
        self.addChildViewController(controller)
        video.play()
        
        do
        {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
        }
        catch let error
        {
            print("error")
        }
                NSNotificationCenter.defaultCenter().addObserver(self,
                    selector: "playerItemDidReachEnd:",
                    name: AVPlayerItemDidPlayToEndTimeNotification,
                    object: video.currentItem)

    }
    
}
