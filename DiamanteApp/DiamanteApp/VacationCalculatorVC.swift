//
//  VacationCalculatorVC.swift
//  DiamanteApp
//
//  Created by mrinal khullar on 10/18/16.
//  Copyright © 2016 mrinal khullar. All rights reserved.
//

import UIKit
import MessageUI

class VacationCalculatorVC: UIViewController,UITextFieldDelegate,MFMailComposeViewControllerDelegate
{
//OUTLETS OF SUB VIEWS
    
    @IBOutlet weak var textfield1: UITextField!
    @IBOutlet weak var text2: UITextField!
    @IBOutlet weak var textfield3: UITextField!
    @IBOutlet weak var textfield4: UITextField!
    @IBOutlet weak var textfield5: UITextField!
    
    
    @IBOutlet weak var BlackBlurView: UIView!
    @IBOutlet weak var SideBarMenuView: UIView!
    @IBOutlet weak var SideBarBtn: UIButton!
    @IBOutlet weak var LyncWithHospitalBtn: UIButton!
    @IBOutlet weak var LyncWithHospitalLbl: UILabel!
    @IBOutlet weak var MyCareerGoalsBtn: UIButton!
    @IBOutlet weak var MyCareerGoalsLbl: UILabel!
    @IBOutlet weak var MyExperienceBtn: UIButton!
    @IBOutlet weak var MyExperienceLbl: UILabel!
    @IBOutlet weak var CreateProfileBtn: UIButton!
    @IBOutlet weak var CreateProfileLbl: UILabel!
    @IBOutlet weak var MyProfileLbl: UILabel!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.textfield1.borderStyle = UITextBorderStyle.RoundedRect
        self.text2.borderStyle = UITextBorderStyle.RoundedRect
        self.textfield3.borderStyle = UITextBorderStyle.RoundedRect
        self.textfield4.borderStyle = UITextBorderStyle.RoundedRect
        self.textfield5.borderStyle = UITextBorderStyle.RoundedRect
        
    
        
        var gestureRecognizer = UITapGestureRecognizer(target: self, action:Selector("swipeHandler"))
        gestureRecognizer.numberOfTapsRequired = 1
        //gestureRecognizer.direction = UISwipeGestureRecognizerDirection.Right
        self.BlackBlurView.addGestureRecognizer(gestureRecognizer)
        
    }
    func swipeHandler ()
    {
        
        print("Swipe received.")
        
        self.BlackBlurView.hidden = true
        
        UIView.animateWithDuration(0.4, delay: 0.0, options: UIViewAnimationOptions.TransitionFlipFromRight, animations: { () -> Void in
            
            self.SideBarMenuView.frame.origin.x = self.view.frame.size.width
            
            }, completion: { (finished: Bool) -> Void in
                
                // you can do this in a shorter, more concise way by setting the value to its opposite, NOT value
                
                
        })
        
        
    }
    override func viewWillAppear(animated: Bool)
    {
        
        self.SideBarMenuView.frame.origin.x = self.view.frame.size.width
        self.BlackBlurView.hidden = true
        
    }
    
    
    


    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        
    textField.resignFirstResponder()
    return true
    
    }
   
    @IBAction func BackButton(sender: AnyObject)
    {
        
    self.navigationController?.popViewControllerAnimated(true)
        
    }
    
    @IBAction func MenuButton(sender: AnyObject)
    {
        
        self.sendEmail()
        
//        self.BlackBlurView.hidden = false
//        
//        
//        UIView.animateWithDuration(0.4, delay: 0.0, options: UIViewAnimationOptions.TransitionFlipFromRight, animations: { () -> Void in
//            
//            self.SideBarMenuView.frame.origin.x = self.view.frame.size.width / 2
//            
//            
//            }, completion: { (finished: Bool) -> Void in
//                
//                // you can do this in a shorter, more concise way by setting the value to its opposite, NOT value
//                
//                
//                
//        })
        
    }
    
    @IBAction func SideBarBtnInMenu(sender: AnyObject)
    {
        print("SideBarBtnInMenu")
        
        self.BlackBlurView.hidden = true
        
        UIView.animateWithDuration(0.4, delay: 0.0, options: UIViewAnimationOptions.TransitionFlipFromRight, animations: { () -> Void in
            
            self.SideBarMenuView.frame.origin.x = self.view.frame.size.width
            
            }, completion: { (finished: Bool) -> Void in
                
                // you can do this in a shorter, more concise way by setting the value to its opposite, NOT value
                
                
        })
        
        
    }
    
    
    @IBAction func CreateProfileAction(sender: AnyObject)
    {
        print("CreateProfileAction")
    }
    
    
    
    @IBAction func MyExperienceAction(sender: AnyObject)
    {
        print("MyExperienceAction")
    }
    
    
    @IBAction func MyCareerGoalsAction(sender: AnyObject)
    {
        print("MyCareerGoalsAction")
    }
    
    
    @IBAction func LyncWithHospitalAction(sender: AnyObject)
    {
        print("LyncWithHospitalAction")
    }
    
    
    //MFMAIL VIEW CONTROLLER DELEGATES
    
    func sendEmail()
    {
        
        let path = NSBundle.mainBundle().pathForResource("video-1@3x", ofType:"png")
        
        let getImagePath = NSURL(fileURLWithPath: path!)
        
        var controller = MFMailComposeViewController()
        
        controller.mailComposeDelegate = self
        controller.setSubject("Green card application")
        controller.setMessageBody("Hi , <br/>  This is my new latest designed green card.", isHTML: true)
        controller.setToRecipients(["brstdev3@gmail.com"])
        controller.addAttachmentData(NSData(contentsOfURL: getImagePath)!, mimeType: "png", fileName: "My Green Card.png")
        
            self.presentViewController(controller, animated: true, completion:nil)
        
       // controller.rel

    }
    
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?)
    {
        if result == MFMailComposeResultSent
        {
            
            print("It's away!")
            
        }
        
        self.dismissViewControllerAnimated(true, completion:nil)
        
    }
    
 

    
}
