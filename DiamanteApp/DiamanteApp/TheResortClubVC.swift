//
//  TheResortClubVC.swift
//  DiamanteApp
//
//  Created by mrinal khullar on 10/13/16.
//  Copyright © 2016 mrinal khullar. All rights reserved.
//

import UIKit

class TheResortClubVC: BaseContentViewController,UIScrollViewDelegate
{

    //VARIABLES DECLARATION
    
    var previouscontentvc = 0
    
    var viewforimageframe = UIView()
    
    //OUTLETS OF SUBVIEWS
    
    @IBOutlet weak var BlackBlurView: UIView!
    @IBOutlet weak var SideBarMenuView: UIView!
    @IBOutlet weak var SideBarBtn: UIButton!
    @IBOutlet weak var LyncWithHospitalBtn: UIButton!
    @IBOutlet weak var LyncWithHospitalLbl: UILabel!
    @IBOutlet weak var MyCareerGoalsBtn: UIButton!
    @IBOutlet weak var MyCareerGoalsLbl: UILabel!
    @IBOutlet weak var MyExperienceBtn: UIButton!
    @IBOutlet weak var MyExperienceLbl: UILabel!
    @IBOutlet weak var CreateProfileBtn: UIButton!
    @IBOutlet weak var CreateProfileLbl: UILabel!
    @IBOutlet weak var MyProfileLbl: UILabel!
    
    
    @IBOutlet weak var scroll_view: UIScrollView!
    @IBOutlet weak var main_image: UIImageView!
    
    @IBOutlet weak var bottom_view: UIView!
    
    @IBOutlet weak var back_image: UIImageView!
    @IBOutlet weak var middle_label: UILabel!
    @IBOutlet weak var title_label: UILabel!
    @IBOutlet weak var description_label: UILabel!
    @IBOutlet weak var menu_label: UIButton!
    @IBOutlet weak var description_label2: UILabel!
    
    
    override func viewDidLoad()
    {
        
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.automaticallyAdjustsScrollViewInsets = false
        
        var gestureRecognizer = UITapGestureRecognizer(target: self, action:Selector("swipeHandler"))
        gestureRecognizer.numberOfTapsRequired = 1
        
        //gestureRecognizer.direction = UISwipeGestureRecognizerDirection.Right
        self.BlackBlurView.addGestureRecognizer(gestureRecognizer)
        
       // self.rootViewController.view.userInteractionEnabled = false
        
        var gestureforimage = UITapGestureRecognizer(target: self, action: Selector("imagetap"))
        gestureforimage.numberOfTapsRequired = 1
        self.main_image.addGestureRecognizer(gestureforimage)
        
        self.main_image.userInteractionEnabled = true
        
        viewforimageframe.frame = self.main_image.frame
        
        viewforimageframe.backgroundColor = UIColor.yellowColor()
        
        self.view.addSubview(viewforimageframe)
        
        viewforimageframe.hidden = true
        
        
// SCROLL TO ZOOM IMAGE
        
        self.scroll_view.minimumZoomScale = 1
        self.scroll_view.maximumZoomScale = 2.3
        self.scroll_view.contentSize = self.main_image!.frame.size
     //   self.scroll_view.delegate! = self
      
        
        
    }
    func swipeHandler ()
    {
        
        print("Swipe received.")
        
        self.BlackBlurView.hidden = true
        
        UIView.animateWithDuration(0.4, delay: 0.0, options: UIViewAnimationOptions.TransitionFlipFromRight, animations: { () -> Void in
            
            self.SideBarMenuView.frame.origin.x = self.view.frame.size.width
            
            }, completion: { (finished: Bool) -> Void in
                
                // you can do this in a shorter, more concise way by setting the value to its opposite, NOT value
                
                
        })
        
        
    }
    func imagetap()
    {
        
        self.scroll_view.contentSize = self.main_image.frame.size
        
        print(self.main_image.frame)
        print(self.scroll_view.frame)
        print(self.scroll_view.contentSize)
        
        if(main_image.frame.size.width < self.view.frame.size.width)
        {
            
            self.scroll_view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)
            self.main_image.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)
            self.bottom_view.hidden = true
            self.back_image.hidden = true
            self.middle_label.hidden = true
            self.title_label.hidden = true
            self.description_label.hidden = true
            self.menu_label.hidden = true
            self.description_label2.hidden = true
            
        }
        else if(main_image.frame.size.width == self.view.frame.size.width)
        {
            
            self.scroll_view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)
            self.main_image.frame = self.viewforimageframe.frame
            self.bottom_view.hidden = false
            self.back_image.hidden = false
            self.middle_label.hidden = false
            self.title_label.hidden = false
            self.description_label.hidden = false
            self.menu_label.hidden = false
            self.description_label2.hidden = false
          
            
            
        }
        
//        print(self.main_image.frame)
//        print(self.scroll_view.frame)
//        print(self.scroll_view.contentSize)
        
    }
    override func didReceiveMemoryWarning()
    {
        
        super.didReceiveMemoryWarning()
        
    }
    override func viewWillAppear(animated: Bool)
    {
        
        self.SideBarMenuView.frame.origin.x = self.view.frame.size.width
        self.BlackBlurView.hidden = true
        
    }

    
    @IBAction func BackButton(sender: AnyObject)
    {
    
        self.navigationController?.popViewControllerAnimated(true)

    }

    @IBAction func MenuButton(sender: AnyObject)
    {
        
        self.BlackBlurView.hidden = false
        
        
        UIView.animateWithDuration(0.4, delay: 0.0, options: UIViewAnimationOptions.TransitionFlipFromRight, animations: { () -> Void in
            
            self.SideBarMenuView.frame.origin.x = self.view.frame.size.width / 2
            
            
            }, completion: { (finished: Bool) -> Void in
                
                // you can do this in a shorter, more concise way by setting the value to its opposite, NOT value
                
                
                
        })
        
        
    }
    
    @IBAction func SideBarBtnInMenu(sender: AnyObject)
    {
        print("SideBarBtnInMenu")
        
        self.BlackBlurView.hidden = true
        
        UIView.animateWithDuration(0.4, delay: 0.0, options: UIViewAnimationOptions.TransitionFlipFromRight, animations: { () -> Void in
            
            self.SideBarMenuView.frame.origin.x = self.view.frame.size.width
            
            }, completion: { (finished: Bool) -> Void in
                
                // you can do this in a shorter, more concise way by setting the value to its opposite, NOT value
                
                
        })
        
        
    }
    
    
    @IBAction func CreateProfileAction(sender: AnyObject)
    {
        print("CreateProfileAction")
    }
    
    
    
    @IBAction func MyExperienceAction(sender: AnyObject)
    {
        print("MyExperienceAction")
    }
    
    
    @IBAction func MyCareerGoalsAction(sender: AnyObject)
    {
        print("MyCareerGoalsAction")
    }
    
    
    @IBAction func LyncWithHospitalAction(sender: AnyObject)
    {
        print("LyncWithHospitalAction")
    }
    
    
// SCROLL VIEW DELEGATES
    
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView?
    {
//        if(self.main_image.frame.origin.x =! 0)
//        {
        
        if(main_image.frame.size.width < self.view.frame.size.width)
        {
        
        }
        else
        {
//        return self.main_image!
            return nil
        }
        return nil
        
       // }
    }
    
    func scrollViewDidEndZooming(scrollView: UIScrollView, withView view: UIView?, atScale scale: CGFloat)
    {
        
    }

}
