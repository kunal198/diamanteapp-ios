//
//  VideoCategoriesVC.swift
//  DiamanteApp
//
//  Created by mrinal khullar on 10/20/16.
//  Copyright © 2016 mrinal khullar. All rights reserved.
//

import UIKit

class VideoCategoriesVC: UIViewController , UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout
{

// OUTLETS OF SUBVIEWS
    
    
    @IBOutlet weak var collection_view: UICollectionView!
    
    
    
    
// VARIABLE DECLARATION 
    
    var arrayofimages1 = [UIImage]()
    var arrayoflabel = NSMutableArray()
    
    var image_var = UIImage()
    
    
    @IBOutlet weak var BlackBlurView: UIView!
    @IBOutlet weak var SideBarMenuView: UIView!
    @IBOutlet weak var SideBarBtn: UIButton!
    @IBOutlet weak var LyncWithHospitalBtn: UIButton!
    @IBOutlet weak var LyncWithHospitalLbl: UILabel!
    @IBOutlet weak var MyCareerGoalsBtn: UIButton!
    @IBOutlet weak var MyCareerGoalsLbl: UILabel!
    @IBOutlet weak var MyExperienceBtn: UIButton!
    @IBOutlet weak var MyExperienceLbl: UILabel!
    @IBOutlet weak var CreateProfileBtn: UIButton!
    @IBOutlet weak var CreateProfileLbl: UILabel!
    @IBOutlet weak var MyProfileLbl: UILabel!
    
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        let flow_layout = UICollectionViewFlowLayout()
        
        flow_layout.minimumInteritemSpacing = 15
        flow_layout.minimumLineSpacing = 8
        flow_layout.sectionInset.left = 15
        flow_layout.sectionInset.right = 15
        
        collection_view.collectionViewLayout = flow_layout

        arrayofimages1 = [UIImage(named: "video-1@3x")!,UIImage(named: "video-2@3x")!,UIImage(named: "video-3@3x")!,UIImage(named: "video-4@3x")!,UIImage(named: "video-5@3x")!,UIImage(named: "video-6@3x")!]
        
        arrayoflabel = ["CRYSTAL LAGOON","DAVIS LOVE-III DUNES GOLF","DIAMANTE 2016","DIAMNATE FAMILY","DIAMANTE VIDEO","EL CARDONAL VIDEO"]
        
        var gestureRecognizer = UITapGestureRecognizer(target: self, action:Selector("swipeHandler"))
        gestureRecognizer.numberOfTapsRequired = 1
        //gestureRecognizer.direction = UISwipeGestureRecognizerDirection.Right
        self.BlackBlurView.addGestureRecognizer(gestureRecognizer)
        
    }
    func swipeHandler ()
    {
        
        print("Swipe received.")
        
        self.BlackBlurView.hidden = true
        
        UIView.animateWithDuration(0.4, delay: 0.0, options: UIViewAnimationOptions.TransitionFlipFromRight, animations: { () -> Void in
            
            self.SideBarMenuView.frame.origin.x = self.view.frame.size.width
            
            }, completion: { (finished: Bool) -> Void in
                
                // you can do this in a shorter, more concise way by setting the value to its opposite, NOT value
                
                
        })
        
        
    }
    override func viewWillAppear(animated: Bool)
    {
        
        self.SideBarMenuView.frame.origin.x = self.view.frame.size.width
        self.BlackBlurView.hidden = true
        
    }
    


    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func BackButton(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    @IBAction func MenuButton(sender: AnyObject)
    {
        self.BlackBlurView.hidden = false
        
        
        UIView.animateWithDuration(0.4, delay: 0.0, options: UIViewAnimationOptions.TransitionFlipFromRight, animations: { () -> Void in
            
            self.SideBarMenuView.frame.origin.x = self.view.frame.size.width / 2
            
            
            }, completion: { (finished: Bool) -> Void in
                
                // you can do this in a shorter, more concise way by setting the value to its opposite, NOT value
                
                
                
        })
    }
    
    
    
    
// COLLECTION VIEW DELEGATES AND DATASOURCE
    
    
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        
        return 6
        
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int
    {
        
        return 1
        
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("videocell", forIndexPath: indexPath) as! VideoCell
        
        cell.image_view.image = arrayofimages1[indexPath.row]
        
        cell.iconimage.image = UIImage(named: "videocellicon")
        
        cell.label_cell.text = arrayoflabel[indexPath.row] as? String
        
        
        cell.setNeedsLayout()
        
        cell.layoutIfNeeded()
        
        cell.layoutSubviews()
        
//        cell.label1.text = arrayoflabel1[indexPath.row]
//        cell.label2.text = arrayoflabel2[indexPath.row]
//        cell.imageview.image = arrayofimages1[indexPath.row]
//        
//        cell.label1.font = cell.label1.font.fontWithSize(28)
//        cell.label2.font = cell.label2.font.fontWithSize(28)
        
        cell.layer.cornerRadius = 4
        cell.view_on_cell.layer.borderWidth = 2
        cell.view_on_cell.layer.borderColor = UIColor.whiteColor().CGColor
        
        cell.image_view.frame.origin.x = 0
        cell.image_view.frame.origin.y = 0
        cell.image_view.frame.size.height = cell.frame.size.height
        cell.image_view.frame.size.width = cell.frame.size.width
        
        
        return cell
        
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        
        let screensize = self.view.frame.size
        
        let screenwidth = screensize.width
        
        let cellwiddth = screenwidth/2 - 22.5
        
        var cellheight = CGFloat()
        
        cellheight = (collection_view.frame.size.height / 3) - 5.3
        
//        if(indexPath.row == 2 || indexPath.row == 3)
//        {
//            cellheight = (collection_view.frame.size.height/3) - 4
//        }
//        else
//        {
//            cellheight = (collection_view.frame.size.height/3) - 2
//        }
        
        
        let rectofcell = CGSizeMake(cellwiddth, cellheight)
        
        return rectofcell
        
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
    {

        let obj = self.storyboard?.instantiateViewControllerWithIdentifier("videodes") as! VideoDescriptionVC
        obj.image_var = arrayofimages1[indexPath.row]
        self.navigationController?.pushViewController(obj, animated: true)
        
//        if(indexPath.row == 0)
//        {
//            let obj = self.storyboard?.instantiateViewControllerWithIdentifier("imagevc") as! ImageViewController
//            
//            self.navigationController?.pushViewController(obj, animated: true)
//        }
//            
//        else if(indexPath.row == 1)
//        {
//            
//            let obj = self.storyboard?.instantiateViewControllerWithIdentifier("programbuilder") as! ProgramBuilderVC
//            
//            self.navigationController?.pushViewController(obj, animated: true)
//            
//        }
//            
//        else if(indexPath.row == 2)
//        {
//            
//            let obj = self.storyboard?.instantiateViewControllerWithIdentifier("travel") as! TravelAffiliatesVC
//            
//            self.navigationController?.pushViewController(obj, animated: true)
//            
//        }
//            
//        else if(indexPath.row == 3)
//        {
//            let obj = self.storyboard?.instantiateViewControllerWithIdentifier("vacationvc") as! VacationCalculatorVC
//            
//            self.navigationController?.pushViewController(obj, animated: true)
//        }
//            
//        else if(indexPath.row == 4)
//        {
//            let obj = self.storyboard?.instantiateViewControllerWithIdentifier("paymentvc") as! PaymentCalculatorVC
//            
//            self.navigationController?.pushViewController(obj, animated: true)
//        }
//            
//        else
//        {
//            
//        }
//        
        
        
    }
    @IBAction func SideBarBtnInMenu(sender: AnyObject)
    {
        print("SideBarBtnInMenu")
        
        self.BlackBlurView.hidden = true
        
        UIView.animateWithDuration(0.4, delay: 0.0, options: UIViewAnimationOptions.TransitionFlipFromRight, animations: { () -> Void in
            
            self.SideBarMenuView.frame.origin.x = self.view.frame.size.width
            
            }, completion: { (finished: Bool) -> Void in
                
                // you can do this in a shorter, more concise way by setting the value to its opposite, NOT value
                
                
        })
        
        
    }
    
    
    @IBAction func CreateProfileAction(sender: AnyObject)
    {
        print("CreateProfileAction")
    }
    
    
    
    @IBAction func MyExperienceAction(sender: AnyObject)
    {
        print("MyExperienceAction")
    }
    
    
    @IBAction func MyCareerGoalsAction(sender: AnyObject)
    {
        print("MyCareerGoalsAction")
    }
    
    
    @IBAction func LyncWithHospitalAction(sender: AnyObject)
    {
        print("LyncWithHospitalAction")
    }
    

    
    
}
