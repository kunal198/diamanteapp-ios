//
//  demo1.swift
//  DiamanteApp
//
//  Created by mrinal khullar on 10/15/16.
//  Copyright © 2016 mrinal khullar. All rights reserved.
//

import UIKit

class demo1: UIViewController
{

    @IBOutlet weak var mainview: UIView!
    @IBOutlet weak var scrollview: UIScrollView!
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
     scrollview.contentSize = CGSizeMake(0, self.mainview.frame.height)
        
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func backbutton(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
   

}
