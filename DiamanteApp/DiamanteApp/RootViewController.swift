//
//  RootViewController.swift
//  DiamanteApp
//
//  Created by mrinal khullar on 10/15/16.
//  Copyright © 2016 mrinal khullar. All rights reserved.
//

import UIKit


class RootViewController: UIViewController , UIPageViewControllerDataSource
{
    var pageViewController = UIPageViewController()
    var contentPageRestorationIDs1 = ["trclubvc", "dunesvc", "oceanclubvc","golfvillavc","dunescoarsevc","elcardonalvc"]
    var contentPageRestorationIDs2 = [String]()
    var indexofvc = Int()
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        for var i = indexofvc;i < contentPageRestorationIDs1.count;i++
        {
        contentPageRestorationIDs2.append(contentPageRestorationIDs1[i])
        }
        self.pageViewController = self.storyboard?.instantiateViewControllerWithIdentifier("PageViewController") as! UIPageViewController
        
        self.pageViewController.dataSource = self
        
        let startingViewController = self.viewControllerAtIndex(0)
        
        self.pageViewController.setViewControllers([startingViewController!], direction: .Forward, animated: false, completion: {(finished: Bool) -> Void in
            // Completion code
        })

        self.addChildViewController(self.pageViewController)
        self.view.addSubview(self.pageViewController.view)
        self.pageViewController.didMoveToParentViewController(self)
        
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func viewControllerAtIndex(index: Int) -> UIViewController?
    {
        // Only process a valid index request.
        if index >= self.contentPageRestorationIDs2.count
        {
            return nil
        }
        // Create a new view controller.
        let contentViewController = (self.storyboard!.instantiateViewControllerWithIdentifier(self.contentPageRestorationIDs2[index]) as! BaseContentViewController)
        // Set any data needed by the VC here
        contentViewController.rootViewController = self
        return contentViewController
    }
    
    func goToPreviousContentViewController()
    {
        // Get index of current view controller
        let currentViewController = self.pageViewController.viewControllers![0]
        let vcRestorationID = currentViewController.restorationIdentifier!
        let index = self.contentPageRestorationIDs2.indexOf(vcRestorationID)
        let previousViewController = self.viewControllerAtIndex(index! - 1)
        self.pageViewController.setViewControllers([previousViewController!], direction: .Reverse, animated: true, completion: {(finished: Bool) -> Void in
            // Completion code
        })
    }
    
    func goToNextContentViewController()
    {
        // Get index of current view controller
        let currentViewController = self.pageViewController.viewControllers![0]
        let vcRestorationID = currentViewController.restorationIdentifier!
        let index = self.contentPageRestorationIDs2.indexOf(vcRestorationID)
        let nextViewController = self.viewControllerAtIndex(index! + 1)
        self.pageViewController.setViewControllers([nextViewController!], direction: .Forward, animated: true, completion: {(finished: Bool) -> Void in
            
            // Completion code
            
        })
    }
    
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int
    {
        
        return self.contentPageRestorationIDs2.count
        
    }
    
     func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController?
     {
        
        let vcRestorationID = viewController.restorationIdentifier!
        let index = self.contentPageRestorationIDs2.indexOf(vcRestorationID)
        if index == 0
        {
            
            return nil
            
        }
        
        return self.viewControllerAtIndex(index! - 1)
        
    }
    
     func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController?
     {
        
        let vcRestorationID = viewController.restorationIdentifier!
        let index = self.contentPageRestorationIDs2.indexOf(vcRestorationID)
        if index == self.contentPageRestorationIDs2.count - 1
        {
            
            return nil
            
            
        }
        return self.viewControllerAtIndex(index! + 1)
        
    }
    
    
    
    
}
