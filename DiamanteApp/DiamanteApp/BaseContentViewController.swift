//
//  BaseContentViewController.swift
//  DiamanteApp
//
//  Created by mrinal khullar on 10/15/16.
//  Copyright © 2016 mrinal khullar. All rights reserved.
//

import UIKit

class BaseContentViewController: UIViewController
{
    
    var rootViewController = RootViewController()
    
    override func viewDidLoad()
    {
        
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }

    override func didReceiveMemoryWarning()
    {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    

}
