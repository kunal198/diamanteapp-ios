//
//  CategoriesCollectionViewCell.swift
//  DiamanteApp
//
//  Created by mrinal khullar on 10/13/16.
//  Copyright © 2016 mrinal khullar. All rights reserved.
//

import UIKit

class CategoriesCollectionViewCell: UICollectionViewCell
{
    
    @IBOutlet weak var image_view: UIImageView!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
}
