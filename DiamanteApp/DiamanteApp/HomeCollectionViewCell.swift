//
//  HomeCollectionViewCell.swift
//  DiamanteApp
//
//  Created by mrinal khullar on 10/13/16.
//  Copyright © 2016 mrinal khullar. All rights reserved.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell
{
    
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var imageview: UIImageView!
    
}
