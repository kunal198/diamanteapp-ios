//
//  ELCardonalVC.swift
//  DiamanteApp
//
//  Created by mrinal khullar on 10/14/16.
//  Copyright © 2016 mrinal khullar. All rights reserved.
//

import UIKit

class ELCardonalVC: BaseContentViewController
{
//VARIABLES DEVLARATION
    var previouscontentvc = 0
    var viewforimageframe = UIView()
    
//OUTLETS OF SUBVIEWS
    
    @IBOutlet weak var BlackBlurView: UIView!
    @IBOutlet weak var SideBarMenuView: UIView!
    @IBOutlet weak var SideBarBtn: UIButton!
    @IBOutlet weak var LyncWithHospitalBtn: UIButton!
    @IBOutlet weak var LyncWithHospitalLbl: UILabel!
    @IBOutlet weak var MyCareerGoalsBtn: UIButton!
    @IBOutlet weak var MyCareerGoalsLbl: UILabel!
    @IBOutlet weak var MyExperienceBtn: UIButton!
    @IBOutlet weak var MyExperienceLbl: UILabel!
    @IBOutlet weak var CreateProfileBtn: UIButton!
    @IBOutlet weak var CreateProfileLbl: UILabel!
    @IBOutlet weak var MyProfileLbl: UILabel!
    

    @IBOutlet weak var menu_button: UIButton!
    @IBOutlet weak var title_label: UILabel!
    @IBOutlet weak var back_image: UIImageView!
    @IBOutlet weak var back_button: UIButton!
    @IBOutlet weak var main_image: UIImageView!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var middle_label: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var bottom_view: UIView!
    override func viewDidLoad()
    {
        
        super.viewDidLoad()

        // Do any additional setup after loading the view.
         self.automaticallyAdjustsScrollViewInsets = false
        
        self.SideBarMenuView.hidden = true
        
        var gestureRecognizer = UITapGestureRecognizer(target: self, action:Selector("swipeHandler"))
        gestureRecognizer.numberOfTapsRequired = 1
        //gestureRecognizer.direction = UISwipeGestureRecognizerDirection.Right
        self.BlackBlurView.addGestureRecognizer(gestureRecognizer)
        
        
        
        
        var gestureforimage = UITapGestureRecognizer(target: self, action: Selector("imagetap"))
        gestureforimage.numberOfTapsRequired = 1
        self.main_image.addGestureRecognizer(gestureforimage)
        
        self.main_image.userInteractionEnabled = true
        
        viewforimageframe.frame = self.main_image.frame
        
        viewforimageframe.backgroundColor = UIColor.yellowColor()
        
        self.view.addSubview(viewforimageframe)
        
        viewforimageframe.hidden = true
        

    }
    func swipeHandler ()
    {
        
        print("Swipe received.")
        
        self.BlackBlurView.hidden = true
        
        UIView.animateWithDuration(0.4, delay: 0.0, options: UIViewAnimationOptions.TransitionFlipFromRight, animations: { () -> Void in
            
            self.SideBarMenuView.frame.origin.x = self.view.frame.size.width
            
            }, completion: { (finished: Bool) -> Void in
                
                // you can do this in a shorter, more concise way by setting the value to its opposite, NOT value
                
                self.SideBarMenuView.hidden = true
        })
        
        
    }
    
    func imagetap()
    {
        
//        self.scroll_view.contentSize = self.main_image.frame.size
//        
//        print(self.main_image.frame)
//        print(self.scroll_view.frame)
//        print(self.scroll_view.contentSize)
        
        if(main_image.frame.size.width < self.view.frame.size.width)
        {
            
          //  self.scroll_view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)
            self.main_image.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)
            self.bottom_view.hidden = true
            self.back_button.hidden = true
            self.label1.hidden = true
            self.title_label.hidden = true
            self.label2.hidden = true
            self.menu_button.hidden = true
            self.middle_label.hidden = true
            
        }
        else if(main_image.frame.size.width == self.view.frame.size.width)
        {
            
          //  self.scroll_view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)
            self.main_image.frame = self.viewforimageframe.frame
            self.bottom_view.hidden = false
            self.back_button.hidden = false
            self.label1.hidden = false
            self.title_label.hidden = false
            self.label2.hidden = false
            self.menu_button.hidden = false
            self.middle_label.hidden = false
            
            
            
        }
        
        //        print(self.main_image.frame)
        //        print(self.scroll_view.frame)
        //        print(self.scroll_view.contentSize)
        
    }
    

    
    override func didReceiveMemoryWarning()
    {
        
        super.didReceiveMemoryWarning()
        
    }
    override func viewWillAppear(animated: Bool)
    {
        
        self.SideBarMenuView.frame.origin.x = self.view.frame.size.width
        self.BlackBlurView.hidden = true
        
        
        
        if(num5 != 1)
        {
            self.main_image.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)
            self.bottom_view.hidden = true
            self.back_button.hidden = true
            self.label1.hidden = true
            self.title_label.hidden = true
            self.label2.hidden = true
            self.menu_button.hidden = true
            self.middle_label.hidden = true

        }

    }
    

    @IBAction func BackButton(sender: AnyObject)
    {
        
        
        if(num5 == 1)
        {
            self.navigationController?.popViewControllerAnimated(true)
            num5 = 0
        }
        else
        {
            self.rootViewController.goToPreviousContentViewController()
        }

    }
    
    @IBAction func MenuButton(sender: AnyObject)
    {
        self.BlackBlurView.hidden = false
        self.SideBarMenuView.hidden = false
        
        UIView.animateWithDuration(0.4, delay: 0.0, options: UIViewAnimationOptions.TransitionFlipFromRight, animations: { () -> Void in
            
            self.SideBarMenuView.frame.origin.x = self.view.frame.size.width / 2
            
            
            }, completion: { (finished: Bool) -> Void in
                
                // you can do this in a shorter, more concise way by setting the value to its opposite, NOT value
                
                
                
        })
        

    }
    
    @IBAction func SideBarBtnInMenu(sender: AnyObject)
    {
        print("SideBarBtnInMenu")
        
        self.BlackBlurView.hidden = true
        
        UIView.animateWithDuration(0.4, delay: 0.0, options: UIViewAnimationOptions.TransitionFlipFromRight, animations: { () -> Void in
            
            self.SideBarMenuView.frame.origin.x = self.view.frame.size.width
            
            }, completion: { (finished: Bool) -> Void in
                
                // you can do this in a shorter, more concise way by setting the value to its opposite, NOT value
              self.SideBarMenuView.hidden = true
                
        })
        
        
    }
    
    
    @IBAction func CreateProfileAction(sender: AnyObject)
    {
        print("CreateProfileAction")
    }
    
    
    
    @IBAction func MyExperienceAction(sender: AnyObject)
    {
        print("MyExperienceAction")
    }
    
    
    @IBAction func MyCareerGoalsAction(sender: AnyObject)
    {
        print("MyCareerGoalsAction")
    }
    
    
    @IBAction func LyncWithHospitalAction(sender: AnyObject)
    {
        print("LyncWithHospitalAction")
    }


}
